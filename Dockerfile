FROM ubuntu:16.04

# ENV DEBIAN_FRONTEND=noninteractive is discouraged in Docker FAQ
# see: https://docs.docker.com/engine/faq/#why-is-debian_frontendnoninteractive-discouraged-in-dockerfiles
#
RUN set -eux; \
    apt-get update; \
    apt-get install -y tzdata; \
    ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime; \
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure tzdata;  # DEBIAN_FRONTEND supperss dpkg-reconfigure interactive input.

# install ubuntu basic tools
RUN set -eux; \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    tree \
    git \
    jq \
    less colordiff \
    curl wget netcat iputils-ping net-tools iproute2 \
    apache2-utils \
    ruby \
    python3-setuptools python3-pip

RUN pip3 install --upgrade pip
# install awscli
RUN pip3 install --no-cache-dir awscli

# install other
RUN set -eux; \
    apt-get install -y --no-install-recommends \
    vim-tiny \
    screen \
    tmux \
    tcpdump

RUN set -eux; \
    apt-get install -y --no-install-recommends \
    nginx \
    freeradius \
    rsyslog

# ----------------------------------------------------------------------
COPY files/watchdog.sh /usr/local/bin/

# ----------------------------------------------------------------------
# add user ubuntu
RUN set -eux; \
    groupadd --gid 1000 ubuntu; \
    useradd --gid 1000 -G adm,audio,plugdev,sudo,video --create-home --shell /bin/bash --uid 1000 ubuntu

# install sudo (DANGER!)
COPY files/sudoers.d/ubuntu /etc/sudoers.d/
RUN set -eux; \
    apt-get install -y --no-install-recommends sudo
# https://github.com/sudo-project/sudo/issues/42
# https://bugzilla.redhat.com/show_bug.cgi?id=1773148
RUN echo "Set disable_coredump false" >> /etc/sudo.conf

# ----------------------------------------------------------------------

USER 1000
ENV HOME /home/ubuntu
WORKDIR /home/ubuntu

# ----------------------------------------------------------------------
#CMD ["watchdog.sh"]
CMD [ "tail", "-f", "/dev/null" ]
