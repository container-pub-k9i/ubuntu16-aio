#!/bin/bash
set -euo pipefail

SLEEP=5
[[ -n ${1:-} ]] && SLEEP=$1
while true; do
  DT=$(date)
  echo "# ==> INFO: $DT; Sleep ${SLEEP} +5 sec in loop."
  w
  
  if ! read -t ${SLEEP} line; then
    sleep 5  # for safe
    continue  # timeout to continue
  fi
  
  # non timeout
  echo "# ==> INFO: Type any character in ${SLEEP} sec to exit"
  if read -t ${SLEEP} line; then
    [[ -n $line ]] && exit
  fi
done
